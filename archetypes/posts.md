---
title: {{ replace .TranslationBaseName "-" " " | title }}
date: {{ .Date }}
lastmod: {{ .Date }}
cover: /img/cover.png
categories:
  - category1
tags:
  - tag1
  - tag2
draft: true
---

Cut out summary from your post content here.

<!--more-->

The remaining content of your post.
